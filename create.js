const fs = require("fs");
const createHuman = function (human) {
  fs.writeFileSync("./human.json", JSON.stringify(human));
  return human;
};

const rizki = createHuman({
  name: "Ariawan",
  age: 20,
  address: "Bali",
});

module.exports = createHuman;
