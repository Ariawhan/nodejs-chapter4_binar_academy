const express = require("express");

const app = express();

// const fs = require("fs")

//read file, nama variable nya fs, modulnya "fs"
// const isi = fs.readFileSync('./text.txt', 'utf-8')
// console.log(isi)

//jika di run, akan menambah file txt dengan isi " I Love Binar "
// fs.writeFileSync('./test2.txt', "l love Binar")

const rizki = require("./human.json");
// console.log(rizki)

app.get("/", (req, res) => {
  res.render("index.ejs");
});

app.get("/human", (req, res) => {
  res.render("human.ejs", {
    data: rizki,
  });
});

app.listen(3000); //localhost
